## SYSTEM REQUIREMENTS:

1. UBUNTU 20.04

2. java-8-openjdk [java 1.8.*]

   You can check your java version by typing "java -version"

   -  To download java 1.8, enter following commands:

   ```bash
   $ sudo apt update
   $ sudo apt install openjdk-8-jre-headless
   $ sudo update-alternatives --config java
   ```

   - Now select the alternative having "java-8-openjdk" in its path and press enter.To
     check the java version again type "java -version". Once it is set to 1.8.0 you can
     again start the INSTALLATION process.

3. Around 10 Gb of disk space

4. git 

   ```bash
   sudo apt install git
   ```


5. minicom 

   ```
   sudo apt install minicom -y
   ```

6. An active NFS server. Follow the instructions in the latter part of this document to setup a nfs server.

7. Device-tree-compiler(dtc)

   ```bash
   sudo apt-get install device-tree-compiler
   ```

   ##### Note : If you using Ethernet as your primary network interface to connect to the internet, make sure to download and install all the packages before heading to Board setup.

   ##### This document assumes that your machine(Desktop or laptop) has an Ethernet port and all the above mentioned system requirements are met . Using WiFi is recommended as the board will be interfaced through the Ethernet so to access the internet simultaneously, Wifi can be used. The directory paths mentioned throughout this document are in reference to your ${HOME} directory.



## Setup Demonstration (Video Links)

- [Vega64 Board Setup](https://youtu.be/vvQBSO4SGZU)
- [Eclipse IDE Setup](https://youtu.be/WdSIxPstJDw) for Linux Programming
- [Eclipse IDE Setup](https://youtu.be/gmuFboSs89A) for Bare-metal Programming



## SETUP 

1. Create a workplace directory in your home directory and change the current directory.

   ```bash
   $ cd ${HOME} && mkdir vega64 && cd vega64
   ```

2. Clone the **ECLIPSE IDE** repository.

   `git clone  https://gitlab.com/cdachyd/mdp/eclipse-ide-vega.git`

 3. Clone the **yocto-images** repository to obtain the SDK with the Linux Image and the Root File system.

    `git clone https://gitlab.com/cdachyd/mdp/yocto-images.git`

4. Change the directory to  *${HOME}/vega64/yocto-images/* . Extract *LinuxFlash.tar.gz* . It contains the bbl.bin (Kernel and OpenSBI)**[default binary created by Yocto is fw_payload.bin, here that's renamed as bbl.bin]** , device tree, and few essential files. **[build made from meta-cdac repo hash 4ef08832781b997d246d2fd079a1c3a348493572]**

   ```bash
   $ cd yocto-images
   $ tar -xvf LinuxFlash.tar.gz	
   ```

5. *core-image-minimal-cdac-vega.rootfs.tar.gz* contains the Root Filesystem. Extract it to suitable location for nfs server to mount it. [*I will be using /srv/initramfs as the nfs directory*]

   ```bash
   $ tar -xvf core-image-minimal-cdac-vega.rootfs.tar.gz -C /srv/initramfs/
   $ sync
   ```

6. Execute the *oecore-x86_64-riscv64-toolchain-nodistro.0.sh* script to setup the Yocto SDK. Give **${HOME}/vega64/sdk** as the installation path and proceed as asked.

   ```bash
   $ ./oecore-x86_64-riscv64-toolchain-nodistro.0.sh
   OpenEmbedded SDK installer version nodistro.0
   =============================================
   Enter target directory for SDK (default: /usr/local/oecore-x86_64): ${HOME}/vega64/sdk
   ```

   Proceed and enter Y 

7. After completion of setup you will have the toolchain, sysroots  and an environment setup file in ${HOME}/vega64/sdk .

8. Head to NFS Server Setup and Board Setup before starting with Eclipse IDE.

9. Now change the current directory to *eclipse-ide-vega* Refer to the *User_manual_Linux_IDE.pdf* to work on Eclipse IDE.



## NFS Server Setup

1. To setup the nfs server for mounting the root file system, execute the following commands.

   ```bash
   $ sudo apt update
   $ sudo apt install nfs-kernel-server
   ```

2. Add directory for NFS to */etc/exports* as

   ```bash
   sudo gedit /etc/exports
   ```

   - Add your nfs directory (eg : */srv/initramfs*)  path with exact options shown below. 

     `/srv/initramfs *(rw,nohide,insecure,no_subtree_check,async,no_root_squash)`

3. Stop and Start NFS service by 

   ```bash
   $ sudo service nfs-server stop
   $ sudo service nfs-server start
   ```

4. View status by 

   ```bash
   $ showmount -e
   ```

5. Result will be like 

   ```bash
   Export list :
   /srv/initramfs *
   ```

6. NFS server is active now.

#### Note :
   Also disable the firewall in Ubuntu for proper mounting of nfs filesystem with kernel. 
   ```bash
   sudo ufw disable
   ```


## Board Setup

1. Connect the Vega64 board using the cables provided with the Host computer and power the board.

2. Now On the host machine, change the current directory to *${HOME}/vega64/yocto-images/* and extract LinuxFlash.tar.gz if not done already.

3. Check the network interfaces on the host machine.

   ```bash
   (Ethernet)$ ifconfig
   eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
           inet 10.244.0.120  netmask 255.255.254.0  broadcast 10.244.1.255
           inet6 fe80::2809:883c:67ad:77a3  prefixlen 64  scopeid 0x20<link>
           ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)
           RX packets 19414006  bytes 2200633021 (2.2 GB)
           RX errors 0  dropped 0  overruns 0  frame 0
           TX packets 633355  bytes 119786326 (119.7 MB)
           TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
           device interrupt 20  memory 0xf7200000-f7220000  
   
   lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
           inet 127.0.0.1  netmask 255.0.0.0
           inet6 ::1  prefixlen 128  scopeid 0x10<host>
           loop  txqueuelen 1000  (Local Loopback)
           RX packets 55605  bytes 5218184 (5.2 MB)
           RX errors 0  dropped 0  overruns 0  frame 0
           TX packets 55605  bytes 5218184 (5.2 MB)
           TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
   
   ```

   Notice the interface with **Ethernet** i.e. *eno1* . The above shown output might be different for your machine.

4. Check again if any new interfaces are generated by repeating Step 2. If there is no change, note down the name of the interface with Ethernet (eno1 in the example stated in step 2). Proceed with same instructions if new interface has appeared with respective interface name. 

5. Change the directory to LinuxFlash as mentioned in step 1. Run the shell script sendIP.sh with the interface name and a valid host IP you want to assign to your machine (*OPTIONAL*). By default your machine will be assigned *192.168.1.2* and Vega64 board will be assigned *192.168.1.3*  .

	Example for setting up the custom IP. 
   ```bash
   sudo bash ./setIP.sh eno1 192.168.1.24
   ```
	Or default IP can be set using 
   ```bash
   sudo bash ./setIP.sh eno1 
   ```
	
   [eno1 is the name of interface we noted in step 4. Make sure the IP you provide is in range 192.168.1.0 to 192.168.1.243]

6. The above step will setup the IP's for the board and also the host machine. A device tree (i.e riscv.dtb) and *send.sh* will also be generated in the same directory.

7. Now the nfs directory for the Root File-system is already been done in **SETUP Step 5** . To proceed the *NFS Server Setup* must be completed.

8. To Boot Up the board, 

   - To open the serial console we will use minicom. 

   - To list the ttyUSB devices

     ```bash
     $ dmesg | grep tty
     ```

     Note down the tty interface e.g. *ttyUSB0* . 

   - Start the minicom 

     ```bash
     $ sudo minicom -s
     ```

     ```
                 +-----[configuration]------+
                 | Filenames and paths      |
                 | File transfer protocols  |
                 | Serial port setup        |
                 | Modem and dialing        |
                 | Screen and keyboard      |
                 | Save setup as dfl        |
                 | Save setup as..          |
                 | Exit                     |
                 | Exit from Minicom        |
                 +--------------------------+
     ```

     - Go to *Serial port setup* and edit the *Serial Device* to /dev/ttyUSB0. Hit enter and *Exit* . This will give you a serial console which can be used to access the board. Username is **root** without any password.

     - If */dev/ttyUSB0* does not work, try using */dev/ttyUSB1* .

     - Make sure that the **Hardware Flow Control** in *Serial port setup* mentioned above is **No**
     
   - Change the current directory to *${HOME}/vega64/yocto-images/LinuxFlash*.

     ```bash
     $ ./send.sh
     ```
   This will send the kernel image and device tree binary to the Vega Board using the Ethernet interface. And we can monitor the boot up using serial console using   *minicom*
     

#### Note : 
   Also ensure that the ip used for the Vega board must be consistent. Or if the ip changes, remove the riscv.dtb and run the **setIP.sh**  again with the updated arguments. This will update the  riscv.dts and compile an updated device tree. 


## Getting Started with Eclipse IDE

1. Change the current directory to *${HOME}/vega64/eclipse-ide-vega/IDERelease20201110/*

2. Extract *CDAC_RISC_V_20201110.tar.gz* .

   ```bash
   $ tar -xvf CDAC_RISC_V_20201110.tar.gz 
   ```

3. ```bash
   $ cd CDAC_RISC_V 
   ```

4. The  directory contains the IDE as well as a User manual. Open the User_manual_Linux_IDE.pdf and follow the mentioned steps in the manual.

5. Run the IDE.

   ```bash
   $ ./CDAC_RISC_V 
   ```

   

#### NOTE : 

While creating a project in Eclipse IDE, following characters(separated with commas) should not be used in the project name.

**whitespaces, $ , " , # , % , & , ' , ( , ) , * , + , , , . , / , : , ; , < , = , > , ? , @ , [ , \ , ] , ^ ,` , { , | , } , ~ **

